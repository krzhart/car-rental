## Настройка БД

1. Перейти к файлу по пути **/backend/src/main/resources/application.properties**
2. Изменить строку **spring.datasource.url=jdbc:postgresql://`{host}:{port}/{db_name}`**

БД схема **backend.sql**

---

##

- Для реализации REST Api были добавлены службы (../api/services) и api-контроллеры (../api/controllers). Ответ в виде json. Пример запроса для получения списка свободных автомобилей: `.../api/cars/free`

- Добавлены службы (../services), которые по средставм http запросов получают данные.
- Добавлены view-контроллеры (../controllers) для передачи полученых данных html страницам.

---

- Для стилизации html-страниц использовался bootstrap и DataTables (для таблиц);