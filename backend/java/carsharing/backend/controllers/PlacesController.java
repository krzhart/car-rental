package carsharing.backend.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.CarDetails;
import carsharing.backend.models.Car;
import carsharing.backend.models.Place;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.PlacesRepo;
import carsharing.backend.services.CarService;
import carsharing.backend.services.ContractService;
import carsharing.backend.services.PlaceService;


@Controller
public class PlacesController {	
	private PlaceService placeService;
	
	public PlacesController(
			PlaceService placeService
	) {
		this.placeService = placeService;
	}
	
	// Get list
	@GetMapping("places")
	public String list(Map<String, Object> model) {		
		List<Place> places = placeService.list();		
		model.put("places", places);
		
		return "Places/Index";
	}

	// Get by id
	@GetMapping("places/{id}")
	public String get(@PathVariable Integer id, Map<String, Object> model) {
		Place place = placeService.get(id);
		
		if(place == null) {
			return "redirect:/places";
		}		
		
		List<CarDetails> cars = placeService.carList(id);
		model.put("cars", cars);	
		
		model.put("place", place);		
		
		return "Places/View";
	}
		
	// Create
	@GetMapping("places/create")
    public String create(Map<String, Object> model) {        
        return "Places/Create";
    }
    
    @PostMapping("places/create")
    public String create(@RequestParam String address) {   
    	placeService.create(address);    	
        
        return "redirect:/places";
    }
		    

	// Edit
    @GetMapping("places/{id}/edit")
    public String edit(@PathVariable Integer id, Map<String, Object> model) {
    	Place place = placeService.get(id);
		
		if(place == null) {
			return "redirect:/places";
		}	
		
		model.put("place", place);
    	
    	return "Places/Edit";
    }
		    
    @PostMapping("places/{id}/edit")
    public String edit(@PathVariable Integer id, @RequestParam String address) {
    	placeService.update(id, address);	

		return "redirect:/places";
    }
		    
    // Delete
    @GetMapping("places/{id}/delete")
    public String delete(@PathVariable Integer id) {
    	placeService.delete(id);
		
		return "redirect:/places";
    }
}
