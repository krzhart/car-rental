package carsharing.backend.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.models.Car;
import carsharing.backend.models.Contract;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.ContractsRepo;
import carsharing.backend.services.CarService;

@Controller
public class CarsController {	
	private CarService carService;
	
	public CarsController(CarService carService) {
		this.carService = carService;
	}
	
	// Get list
	@GetMapping("cars")
    public String list(Map<String, Object> model) {		
		List<Car> cars= carService.list();  	
        model.put("cars", cars);
        
        return "Cars/Index";
    }
	

	// Get by id
	@GetMapping("cars/{id}")
	public String view(@PathVariable Integer id, Map<String, Object> model) {
		Car car = carService.get(id);
		
		if (car == null) {
			return "redirect:/cars";
		}
		
		model.put("car",  car);
		
		List<Contract> contracts = carService.contractList(id);
		model.put("contracts",  contracts);
		
		return "Cars/View";	
		
	}

	// Create
	@GetMapping("cars/create")
    public String create() {        
        return "Cars/Create";
    }
    
    @PostMapping("cars/create")
    public String create(@RequestParam String carModel,  @RequestParam String carNumber) { 
    	carService.create(carModel, carNumber);    

		return "redirect:/cars";
    }    

	// Edit
    @GetMapping("cars/{id}/edit")
    public String edit(@PathVariable Integer id, Map<String, Object> model) {
    	Car car = carService.get(id);
		
		if (car == null) {
			return "redirect:/cars";
		}
		
		model.put("car",  car);
    	
    	return "Cars/Edit";
    }    
    
    @PostMapping("cars/{id}/edit")
    public String edit(
    		@PathVariable Integer id, 
    		@RequestParam String carModel,
    		@RequestParam String carNumber
	) {
    	carService.update(id, carModel, carNumber);
    	
		return "redirect:/cars";
    }
    
    // Delete
    @GetMapping("cars/{id}/delete")
    public String delete(@PathVariable Integer id) {
    	carService.delete(id);	
		
		return "redirect:/cars";
    }
}
