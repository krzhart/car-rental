package carsharing.backend.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.models.Car;
import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;
import carsharing.backend.models.Place;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.ClientsRepo;	
import carsharing.backend.repos.ContractsRepo;
import carsharing.backend.repos.PlacesRepo;
import carsharing.backend.services.CarService;
import carsharing.backend.services.ClientService;
import carsharing.backend.services.ContractService;
import carsharing.backend.services.PlaceService;

@Controller
public class ContractsController {
	private ContractService contractService; 
	private CarService carService; 
	private ClientService clientService; 
	private PlaceService placeService; 
	
	public ContractsController(
			ContractService contractService,
			CarService carService,
			ClientService clientService,
			PlaceService placeService
	) {
		this.contractService = contractService;
		this.carService = carService;
		this.clientService = clientService;
		this.placeService = placeService;
	}
	
	// Get list
	@GetMapping("contracts")
	public String list(Map<String, Object> model) {
		List<Contract> contracts = contractService.list();
		
		model.put("contracts", contracts);

		return "Contracts/Index";
	}

	// Get by id
	@GetMapping("contracts/{id}")
	public String get(@PathVariable Integer id, Map<String, Object> model) {		
		Contract contract = contractService.get(id);
		
		if(contract == null) {
			return "redirect:/contracts";
		}
		
		model.put("contract", contract);
		
		return "Contracts/View";
	}
		
	// Create
	@GetMapping("contracts/create")
    public String create(Map<String, Object> model) {
		List<Client> clients = clientService.list();
		List<Car> cars = carService.listFree();
		List<Place> places = placeService.list();
		
		model.put("clients", clients);		
		model.put("cars", cars);		
		model.put("places", places);
		
        return "Contracts/Create";
    }
    
    @PostMapping("contracts/create")
    public String create(
    		@RequestParam String created, 
    		@RequestParam String closed,
    		@RequestParam String carId,
    		@RequestParam String clientId,
    		@RequestParam String placeStartId,
    		@RequestParam String placeEndId,
    		Map<String, Object> model
	) {    	
    	contractService.create(created, closed, carId, clientId, placeStartId, placeEndId);
        
        return "redirect:/contracts";
    }	
    
    // Edit
    @GetMapping("contracts/{id}/edit")
    public String edit(@PathVariable Integer id, Map<String, Object> model) {
    	Contract contract = contractService.get(id);
		
		if(contract == null) {
			return "redirect:/contracts";
		}
		
    	if (contract.isClosed()) {
			return "redirect:/contracts";  
    	}
    	
		model.put("contract", contract);
    	
    	return "Contracts/Edit";
    }
		    
    @PostMapping("contracts/{id}/edit")
    public String edit(
    		@PathVariable Integer id,
    		@RequestParam String closed
		) {
    	contractService.update(id, closed);
    	
		return "redirect:/contracts";
    }
    
    // Delete
    @GetMapping("contracts/{id}/delete")
    public String delete(@PathVariable Integer id) {
    	contractService.delete(id);
    	
		return "redirect:/contracts";
    }

}
