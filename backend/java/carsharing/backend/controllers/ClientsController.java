package carsharing.backend.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;
import carsharing.backend.repos.ClientsRepo;
import carsharing.backend.repos.ContractsRepo;
import carsharing.backend.services.ClientService;

@Controller
public class ClientsController {		
	private ClientService clientService;
	
	public ClientsController(ClientService clientService) {
		this.clientService = clientService;
	}

	// Get list
	@GetMapping("clients")
	public String list(Map<String, Object> model) {
		List<Client> clientList = clientService.list();
		model.put("clients", clientList);
		
		return "Clients/Index";
	}

	// Get by id
	@GetMapping("clients/{id}")
	public String get(@PathVariable Integer id, Map<String, Object> model) {
		Client client = clientService.get(id);
		
		if(client == null) {
			return "redirect:/clients";
		}
		
		model.put("client", client);
		
		List<Contract> contracts = clientService.contractList(id);
		model.put("contracts",  contracts);
		
		return "Clients/View";
	}
	
	// Create
	@GetMapping("clients/create")
    public String create() {        
        return "Clients/Create";
    }
    
    @PostMapping("clients/create")
    public String create(
    		@RequestParam String firstName,
    		@RequestParam String lastName,
    		@RequestParam String middleName,
    		Map<String, Object> model
	) {    	
    	clientService.create(firstName, lastName, middleName);
        
        return "redirect:/clients";
    }
	    

	// Edit
    @GetMapping("clients/{id}/edit")
    public String edit(@PathVariable Integer id, Map<String, Object> model) {
Client client = clientService.get(id);
		
		if(client == null) {
			return "redirect:/clients";
		}
		
		model.put("client", client);
    	
    	return "Clients/Edit";
    }
	    
    @PostMapping("clients/{id}/edit")
    public String edit(
    		@PathVariable Integer id, 
    		@RequestParam String firstName,
    		@RequestParam String lastName,
    		@RequestParam String middleName,
    		Map<String, Object> model) {
    	clientService.update(id, firstName, lastName, middleName);

		return "redirect:/clients";
    }
	    
    // Delete
    @GetMapping("clients/{id}/delete")
    public String delete(@PathVariable Integer id, Map<String, Object> model) {
    	clientService.delete(id);	
		
		return "redirect:/clients";
    }
}
