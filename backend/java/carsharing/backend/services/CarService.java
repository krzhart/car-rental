package carsharing.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import carsharing.backend.CarDetails;
import carsharing.backend.models.Car;
import carsharing.backend.models.Contract;

@Service
public class CarService {	
	private String apiUrl = "cars/";
	
	SendRequestService sendRequestService;
	
	public CarService(SendRequestService sendRequestService) {
		this.sendRequestService = sendRequestService;
	}
	
	public List<Car> list() {		
		List<Car> carList = new ArrayList<Car>(); 
		carList = sendRequestService.GetRequest(apiUrl, carList.getClass());
		
		return carList;
	}
	
	public Car get(Integer id) {
		Car car = sendRequestService.GetRequest(apiUrl + id, Car.class);
		
		return car;
	}
	
	public Car create(String carModel, String carNumber) {
		Car car = new Car(carModel, carNumber);
		car = sendRequestService.PostRequest(apiUrl, car, Car.class);
		
		return car;
	}
	
	public Car update(Integer id, String carModel, String carNumber) {
		Car car = new Car(carModel, carNumber);
		car.setId(id);
		car = sendRequestService.PutRequest(apiUrl + id, car, Car.class);
		
		return car;
	}
	
	public void delete(Integer id) {
		sendRequestService.DeleteRequest(apiUrl + id);
	}
	
	public List<Car> listFree() {		
		List<Car> carList = new ArrayList<Car>(); 
		carList = sendRequestService.GetRequest(apiUrl + "/free", carList.getClass());
		
		return carList;
	}
	
	public List<Contract> contractList(Integer id) {		
		List<Contract> contractList = new ArrayList<Contract>(); 
		contractList = sendRequestService.GetRequest(apiUrl + id + "/contracts", contractList.getClass());
		
		return contractList;
	}
}
