package carsharing.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import carsharing.backend.CarDetails;
import carsharing.backend.models.Place;

@Service
public class PlaceService {
	private String apiUrl = "places/";
	
	SendRequestService sendRequestService;
	
	public PlaceService(SendRequestService sendRequestService) {
		this.sendRequestService = sendRequestService;
	}
	
	public List<Place> list() {		
		List<Place> placeList = new ArrayList<Place>(); 
		placeList = sendRequestService.GetRequest(apiUrl, placeList.getClass());
		
		return placeList;
		
	}
	
	public Place get(Integer id) {
		Place place = sendRequestService.GetRequest(apiUrl + id, Place.class);
		
		return place;
	}
	
	public Place create(String address) {
		Place place = new Place(address);
		place = sendRequestService.PostRequest(apiUrl, place, Place.class);
		
		return place;
	}
	
	public Place update(Integer id, String address) {
		Place place = new Place(address);
		place.setId(id);
		place = sendRequestService.PutRequest(apiUrl + id, place, Place.class);
		
		return place;
	}
	
	public void delete(Integer id) {
		sendRequestService.DeleteRequest(apiUrl + id);
	}

	public List<CarDetails> carList(Integer id) {
		List<CarDetails> cars = new ArrayList<CarDetails>();
		cars = sendRequestService.GetRequest(apiUrl + id + "/cars", cars.getClass());
		
		return cars;
	}
}
