package carsharing.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;

@Service
public class ClientService {	
	private String apiUrl = "clients/";
	
	SendRequestService sendRequestService;
	
	public ClientService(SendRequestService sendRequestService) {
		this.sendRequestService = sendRequestService;
	}
	
	public List<Client> list() {		
		List<Client> clientList = new ArrayList<Client>(); 
		clientList = sendRequestService.GetRequest(apiUrl, clientList.getClass());
		
		return clientList;
	}
	
	public Client get(Integer id) {
		Client client = sendRequestService.GetRequest(apiUrl + id, Client.class);
		
		return client;
	}
	
	public Client create(
			String firstName,
    		String lastName,
    		String middleName
	) {
		Client client = new Client(firstName, lastName, middleName);
		client = sendRequestService.PostRequest(apiUrl, client, Client.class);
		
		return client;
	}
	
	public Client update(
			Integer id, 
			String firstName,
    		String lastName,
    		String middleName
	) {
		Client client = new Client(firstName, lastName, middleName);
		client.setId(id);
		client = sendRequestService.PutRequest(apiUrl + id, client, Client.class);
		
		return client;
	}
	
	public void delete(Integer id) {
		sendRequestService.DeleteRequest(apiUrl + id);
	}

	public List<Contract> contractList(Integer id) {		
		List<Contract> contractList = new ArrayList<Contract>(); 
		contractList = sendRequestService.GetRequest(apiUrl + id + "/contracts", contractList.getClass());
		
		return contractList;
	}
}
