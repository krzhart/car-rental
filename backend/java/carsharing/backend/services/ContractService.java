package carsharing.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.ContractCreateRequest;
import carsharing.backend.models.Contract;

@Service
public class ContractService {
	private String apiUrl = "contracts/";
	
	SendRequestService sendRequestService;
	
	public ContractService(SendRequestService sendRequestService) {
		this.sendRequestService = sendRequestService;
	}
	
	public List<Contract> list() {		
		List<Contract> contractList = new ArrayList<Contract>(); 
		contractList = sendRequestService.GetRequest(apiUrl, contractList.getClass());
		
		return contractList;
	}
	
	public Contract get(Integer id) {
		Contract contract = sendRequestService.GetRequest(apiUrl + id, Contract.class);
		
		return contract;
	}
	
	public Contract create(
			String created, 
    		String closed,
    		String carId,
    		String clientId,
    		String placeStartId,
    		String placeEndId
	) {		
		ContractCreateRequest contractCreateRequest = new ContractCreateRequest(created, closed, carId, clientId, placeStartId, placeEndId);
		String ContractCreateResult = sendRequestService.PostRequest(apiUrl, contractCreateRequest);
		
		Contract contract = sendRequestService.parseResponse(ContractCreateResult, Contract.class);
		
		return contract;
	}
	
	public Contract update(
			Integer id,
    		String closed
	) {		
		ContractCreateRequest contractUpdateRequest = new ContractCreateRequest();		
		contractUpdateRequest.setId(id);		
		contractUpdateRequest.setClosed(closed);
		
		String ContractUpdateResult = sendRequestService.PutRequest(apiUrl + id, contractUpdateRequest);
		Contract contract = sendRequestService.parseResponse(ContractUpdateResult, Contract.class);
		
		return contract;
	}
	
	public void delete(Integer id) {
		sendRequestService.DeleteRequest(apiUrl + id);
	}	
}
