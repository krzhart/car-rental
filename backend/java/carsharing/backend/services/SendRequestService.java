package carsharing.backend.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpRequest;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;


@Service
public class SendRequestService {
	String apiUrl = "http://localhost:8080/api/"; 
	
	public <T> T GetRequest(String requestUri, Class<T> type) {
		T result = null;
		try {
			String url = apiUrl + requestUri;
			HttpResponse<String> response = Unirest.get(url)
					.asString();
				
		    ObjectMapper mapper = new ObjectMapper();

		    JsonNode resultObject = mapper.readTree(response.getBody());
			result =  mapper.treeToValue(resultObject, type);
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
		
		return result;
	}
	
	public <T> T PostRequest(String requestUri, T body, Class<T> type) {
		T result = null;
		try {
			String url = apiUrl + requestUri;
									
			ObjectMapper mapper = new ObjectMapper();
			String jsonBody = mapper.writeValueAsString(body);
			
			HttpResponse<String> response = Unirest.post(url)
		            .header("Content-Type", "application/json")
					.body(jsonBody)
					.asString();
				
			JsonNode resultObject = mapper.readTree(response.getBody());
			result =  mapper.treeToValue(resultObject, type);
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
		
		return result;
	}
	
	public <T> String PostRequest(String requestUri, T body) {
		String result = null;
		try {
			String url = apiUrl + requestUri;
									
			ObjectMapper mapper = new ObjectMapper();
			String jsonBody = mapper.writeValueAsString(body);
			
			HttpResponse<String> response = Unirest.post(url)
		            .header("Content-Type", "application/json")
					.body(jsonBody)
					.asString();
				
			result = response.getBody();
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
		
		return result;
	}
	
	public <T> T PutRequest(String requestUri, T body, Class<T> type) {
		T result = null;
		try {
			String url = apiUrl + requestUri;
									
			ObjectMapper mapper = new ObjectMapper();
			String jsonBody = mapper.writeValueAsString(body);

			HttpResponse<String> response = Unirest.put(url)
		            .header("Content-Type", "application/json")
					.body(jsonBody)
					.asString();
				
			JsonNode resultObject = mapper.readTree(response.getBody());
			result =  mapper.treeToValue(resultObject, type);
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
		
		return result;
	}
	
	public <T> String PutRequest(String requestUri, T body) {
		String result = null;
		try {
			String url = apiUrl + requestUri;
									
			ObjectMapper mapper = new ObjectMapper();
			String jsonBody = mapper.writeValueAsString(body);

			HttpResponse<String> response = Unirest.put(url)
		            .header("Content-Type", "application/json")
					.body(jsonBody)
					.asString();
				
			result = response.getBody();
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
		
		return result;
	}
	
	public void DeleteRequest(String requestUri) {
		try {
			String url = apiUrl + requestUri;
												
			Unirest.delete(url).asEmpty();
			
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}	
	}
	
	public <T> T parseResponse(String response, Class<T> type) {
		T result = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode resultObject;
		
			resultObject = mapper.readTree(response);
			result =   mapper.treeToValue(resultObject, type);
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}
		
		return result;
	}
}
