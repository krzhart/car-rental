package carsharing.backend.api.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carsharing.backend.ContractCreateRequest;
import carsharing.backend.api.services.ApiContractService;
import carsharing.backend.exceptions.ConflictException;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Contract;

@RestController
@RequestMapping("api/contracts")
public class ApiContractsController {	
	private ApiContractService apiContractService;
	
	public ApiContractsController(
			ApiContractService apiContractService
			) {
		this.apiContractService = apiContractService;
	}
	
	@GetMapping
	public ResponseEntity<List<Contract>>  list(){		
		List<Contract> contractList = apiContractService.list();
		
		return ResponseEntity.ok(contractList);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Contract> get(@PathVariable Integer id) {
		try {
			Contract contract = apiContractService.get(id);
			return ResponseEntity.ok(contract);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Contract> create(@RequestBody ContractCreateRequest contractCreateRequest){
		try {
			Contract newContract = apiContractService.create(contractCreateRequest);
			
			return ResponseEntity.ok(newContract);
		} catch (ConflictException ex) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
	@PutMapping("{id}")
	public ResponseEntity<Contract> update(@PathVariable Integer id, @RequestBody ContractCreateRequest contractUpdateRequest){
		try {
			Contract editContract = apiContractService.update(id, contractUpdateRequest);
			return ResponseEntity.ok(editContract);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ConflictException ex) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		try {
			apiContractService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
