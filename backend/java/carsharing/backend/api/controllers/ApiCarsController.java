package carsharing.backend.api.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carsharing.backend.CarDetails;
import carsharing.backend.api.services.ApiCarService;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Car;
import carsharing.backend.models.Contract;
import carsharing.backend.models.Place;

@RestController
@RequestMapping("api/cars")
public class ApiCarsController {	
	private ApiCarService apiCarService;
	
	public ApiCarsController(
			ApiCarService apiCarService
			) {
		this.apiCarService = apiCarService;
	}
	
	@GetMapping
	public ResponseEntity<List<Car>>  list(){		
		List<Car> carList = apiCarService.list();
		
		return ResponseEntity.ok(carList);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Car> get(@PathVariable Integer id) {
		try {
			Car car = apiCarService.get(id);
			return ResponseEntity.ok(car);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Car> create(@RequestBody Car car){
		Car newCar = apiCarService.create(car);
		
		return ResponseEntity.ok(newCar);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<Car> update(@PathVariable Integer id, @RequestBody Car car){
		try {
			Car editCar = apiCarService.update(id, car);
			return ResponseEntity.ok(editCar);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		try {
			apiCarService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("free")
	public ResponseEntity<List<Car>> listFree(){		
		List<Car> carList = apiCarService.listFree();
		
		return ResponseEntity.ok(carList);
	}
	
	@GetMapping("{id}/contracts")
	public ResponseEntity<List<Contract>> contractList(@PathVariable Integer id){		
		List<Contract> contractList = apiCarService.contractList(id);
		
		return ResponseEntity.ok(contractList);
	}
}
