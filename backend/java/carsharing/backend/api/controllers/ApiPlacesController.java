package carsharing.backend.api.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carsharing.backend.CarDetails;
import carsharing.backend.api.services.ApiPlaceService;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Place;

@RestController
@RequestMapping("api/places")
public class ApiPlacesController {	
	private ApiPlaceService apiPlaceService;
	
	public ApiPlacesController(
			ApiPlaceService apiPlaceService
			) {
		this.apiPlaceService = apiPlaceService;
	}
	
	@GetMapping
	public ResponseEntity<List<Place>>  getList(){		
		List<Place> placeList = apiPlaceService.list();
		
		return ResponseEntity.ok(placeList);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Place> get(@PathVariable Integer id) {
		try {
			Place place = apiPlaceService.get(id);
			return ResponseEntity.ok(place);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Place> create(@RequestBody Place place){
		Place newPlace = apiPlaceService.create(place);
		
		return ResponseEntity.ok(newPlace);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<Place> update(@PathVariable Integer id, @RequestBody Place place){
		try {
			Place editPlace = apiPlaceService.update(id, place);
			return ResponseEntity.ok(editPlace);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		try {
			apiPlaceService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}	
	
	@GetMapping("{id}/cars")
	public ResponseEntity<List<CarDetails>> listWithRental(@PathVariable Integer id){		
		List<CarDetails> cars = apiPlaceService.carList(id);
		
		return ResponseEntity.ok(cars);
	}
}
