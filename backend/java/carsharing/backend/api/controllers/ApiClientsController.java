package carsharing.backend.api.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import carsharing.backend.api.services.ApiClientService;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;

@RestController
@RequestMapping("api/clients")
public class ApiClientsController {	
	private ApiClientService apiClientService;
	
	public ApiClientsController(
			ApiClientService apiClientService
			) {
		this.apiClientService = apiClientService;
	}
	
	@GetMapping
	public ResponseEntity<List<Client>>  list(){		
		List<Client> clientList = apiClientService.list();
		
		return ResponseEntity.ok(clientList);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Client> get(@PathVariable Integer id) {
		try {
			Client client = apiClientService.get(id);
			return ResponseEntity.ok(client);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping
	public ResponseEntity<Client> create(@RequestBody Client client){
		Client newClient = apiClientService.create(client);
		
		return ResponseEntity.ok(newClient);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<Client> update(@PathVariable Integer id, @RequestBody Client client){
		try {
			Client editClient = apiClientService.update(id, client);
			return ResponseEntity.ok(editClient);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		try {
			apiClientService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotFoundException ex) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("{id}/contracts")
	public ResponseEntity<List<Contract>> contractList(@PathVariable Integer id){		
		List<Contract> contractList = apiClientService.contractList(id);
		
		return ResponseEntity.ok(contractList);
	}
}
