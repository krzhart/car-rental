package carsharing.backend.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;
import carsharing.backend.repos.ClientsRepo;
import carsharing.backend.repos.ContractsRepo;

@Service
public class ApiClientService {
	private ContractsRepo contractsRepo;
	private ClientsRepo clientsRepo;
	
	public ApiClientService(
			ContractsRepo contractsRepo,
			ClientsRepo clientsRepoo
	) {
		this.contractsRepo = contractsRepo;
		this.clientsRepo = clientsRepoo;
	}
	
	public List<Client> list() {		
		Iterable<Client> clients = clientsRepo.findAll();  
		
		List<Client> clientList = new ArrayList<Client>();
		clients.forEach(clientList::add);
		
		return clientList;
	}
	
	public Client get(Integer id) {
		Optional<Client> optionalClient = clientsRepo.findById(id);
		
		if(!optionalClient.isPresent()) {
			throw new NotFoundException();
		}
		
		Client client = optionalClient.get();
		
		return client;
	}
	
	public Client create(Client client) {
		clientsRepo.save(client);
    	
    	return client;
	}
	
	public Client update(Integer id, Client client) {
		Optional<Client> optionalClient = clientsRepo.findById(id);
    	
    	if (!optionalClient.isPresent()) {
    		throw new NotFoundException();
    	}
    	
    	Client editClient = optionalClient.get();
		client.setId(editClient.getId());
		
		clientsRepo.save(client);
    	
    	return client;
	}
	
	public void delete(Integer id) {
		Optional<Client> optionalClient = clientsRepo.findById(id);
    	
    	if (!optionalClient.isPresent()) {
    		throw new NotFoundException();
    	}

    	Client client = optionalClient.get();
    	clientsRepo.delete(client);
	}
	
	public List<Contract> contractList(Integer id) {
		Iterable<Contract> contracts = contractsRepo.findByClientId(id);
		
		List<Contract> contractList = new ArrayList<Contract>();
		contracts.forEach(contractList::add);
		
		return contractList;
	}
}
