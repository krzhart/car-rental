package carsharing.backend.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Car;
import carsharing.backend.models.Contract;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.ContractsRepo;

@Service
public class ApiCarService {
	private ContractsRepo contractsRepo;
	private CarsRepo carsRepo;
	
	public ApiCarService(
			ContractsRepo contractsRepo,
			CarsRepo carsRepoo
	) {
		this.carsRepo = carsRepoo;
		this.contractsRepo = contractsRepo;
	}
	
	public List<Car> list() {		
		Iterable<Car> cars = carsRepo.findAll();  
		
		List<Car> carList = new ArrayList<Car>();
		cars.forEach(carList::add);
		
		return carList;
	}
	
	public Car get(Integer id) {
		Optional<Car> optionalCar = carsRepo.findById(id);
		
		if(!optionalCar.isPresent()) {
			throw new NotFoundException();
		}
		
		Car car = optionalCar.get();
		
		return car;
	}
	
	public Car create(Car car) {
		carsRepo.save(car);
    	
    	return car;
	}
	
	public Car update(Integer id, Car car) {
		Optional<Car> optionalCar = carsRepo.findById(id);
    	
    	if (!optionalCar.isPresent()) {
    		throw new NotFoundException();
    	}
    	
    	Car editCar = optionalCar.get();
		car.setId(editCar.getId());
		
		carsRepo.save(car);
    	
    	return car;
	}
	
	public void delete(Integer id) {
		Optional<Car> optionalCar = carsRepo.findById(id);
    	
    	if (!optionalCar.isPresent()) {
    		throw new NotFoundException();
    	}

    	Car car = optionalCar.get();
    	carsRepo.delete(car);
	}
	
	public List<Car> listFree() {		
		Iterable<Car> cars = carsRepo.findFreeCars();  
		
		List<Car> carList = new ArrayList<Car>();
		cars.forEach(carList::add);
		
		return carList;
	}
	
	public List<Contract> contractList(Integer id) {
		Iterable<Contract> contracts = contractsRepo.findByCarId(id);
		
		List<Contract> contractList = new ArrayList<Contract>();
		contracts.forEach(contractList::add);
		
		return contractList;
	}
}
