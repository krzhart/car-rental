package carsharing.backend.api.services;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.CarDetails;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Car;
import carsharing.backend.models.Contract;
import carsharing.backend.models.Place;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.ContractsRepo;
import carsharing.backend.repos.PlacesRepo;
import carsharing.backend.services.ContractService;
import carsharing.backend.services.SendRequestService;

@Service
public class ApiPlaceService {
	private PlacesRepo placesRepo;
	private CarsRepo carsRepo;
	private ContractsRepo contractsRepo;
	
	public ApiPlaceService(
			PlacesRepo placesRepo,
			CarsRepo carsRepo,
			ContractsRepo contractsRepo
	) {
		this.placesRepo = placesRepo;
		this.carsRepo = carsRepo;
		this.contractsRepo = contractsRepo;
	}
	
	public List<Place> list() {		
		Iterable<Place> places = placesRepo.findAll();
		
		List<Place> placeList = new ArrayList<Place>();
		places.forEach(placeList::add);
		
		return placeList;
	}
	
	public Place get(Integer id) {
		Optional<Place> optionalPlace = placesRepo.findById(id);
		
		if(!optionalPlace.isPresent()) {
			throw new NotFoundException();
		}
		
		Place place = optionalPlace.get();
		
		return place;
	}
	
	public Place create(Place place) {
    	placesRepo.save(place);
    	
    	return place;
	}
	
	public Place update(Integer id, Place place) {
		Optional<Place> optionalPlace = placesRepo.findById(id);
    	
    	if (!optionalPlace.isPresent()) {
    		throw new NotFoundException();
    	}
    	
		Place editPlace = optionalPlace.get();		
		editPlace.setAddress(place.getAddress());	
		
    	placesRepo.save(editPlace);
    	
    	return place;
	}
	
	public void delete(Integer id) {
		Optional<Place> optionalPlace = placesRepo.findById(id);
    	
    	if (!optionalPlace.isPresent()) {
    		throw new NotFoundException();
    	}

		Place place = optionalPlace.get();
    	placesRepo.delete(place);
	}
	
	public List<CarDetails> carList(Integer placeId) {
		Iterable<Car> cars = carsRepo.findAll();
		
		List<CarDetails> carList = new ArrayList<CarDetails>();
		
		for (Car car : cars) {
			carList.add(this.getRentalDuration(placeId, car));
		}
		
		return carList;
	}
	
	private CarDetails getRentalDuration(Integer placeId, Car car){		
		Set<Contract> contracts = contractsRepo.findByPlaceStartIdAndCarId(placeId, car.getId());
		
		Long time = (long) 0;
		Integer count = 0;
		for (Contract contract : contracts) {
			if (contract.isClosed()) {
				Long contractTime = this.calculateDuration(contract.getCreated(), contract.getClosed());
				time += contractTime;
				
				count++;
			}
		}
		
		time /= count;
		
		long day = TimeUnit.MILLISECONDS.toDays(time);        
		long hour = TimeUnit.MILLISECONDS.toHours(time) - (day *24);
		long minute = TimeUnit.MILLISECONDS.toMinutes(time) - (TimeUnit.MILLISECONDS.toHours(time)* 60);
		
		String rentalDuration = String.format("%02d �. %02d �. %02d �.", day, hour, minute);
		
		return  new CarDetails(car, rentalDuration);
	}
	
	private Long calculateDuration(Timestamp createdTimestamp, Timestamp closedTimestamp) {	
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try {
			Date created = format.parse(format.format(createdTimestamp));
			Date closed = format.parse(format.format(closedTimestamp));
			
			return closed.getTime() - created.getTime();
			
		} catch (ParseException e) {
			return (long) 0;
		}
	}

}
