package carsharing.backend.api.services;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import carsharing.backend.CarDetails;
import carsharing.backend.ContractCreateRequest;
import carsharing.backend.exceptions.ConflictException;
import carsharing.backend.exceptions.NotFoundException;
import carsharing.backend.models.Car;
import carsharing.backend.models.Client;
import carsharing.backend.models.Contract;
import carsharing.backend.models.Place;
import carsharing.backend.repos.CarsRepo;
import carsharing.backend.repos.ClientsRepo;
import carsharing.backend.repos.ContractsRepo;
import carsharing.backend.repos.PlacesRepo;

@Service
public class ApiContractService {
	private ContractsRepo contractsRepo;
	private CarsRepo carsRepo;
	private ClientsRepo clientsRepo;
	private PlacesRepo placesRepo;
	
	public ApiContractService(
			ContractsRepo contractsRepo,
			CarsRepo carsRepo,
			ClientsRepo clientsRepo,
			PlacesRepo placesRepo
	) {
		this.contractsRepo = contractsRepo;
		this.carsRepo = carsRepo;
		this.clientsRepo = clientsRepo;
		this.placesRepo = placesRepo;
	}
	
	public List<Contract> list() {		
		Iterable<Contract> contracts = contractsRepo.findAll();
		
		List<Contract> contractList = new ArrayList<Contract>();
		contracts.forEach(contractList::add);
		
		return contractList;
	}
	
	public Contract get(Integer id) {
		Optional<Contract> optionalContract = contractsRepo.findById(id);
		
		if(!optionalContract.isPresent()) {
			throw new NotFoundException();
		}
		
		Contract contract = optionalContract.get();
		
		return contract;
	}
	
	public Contract create(ContractCreateRequest contractCreateRequest) {
		Optional<Car> optionalCar = carsRepo.findById(Integer.parseInt(contractCreateRequest.getCarId()));
    	if (!optionalCar.isPresent()) {
			throw new NotFoundException();
    	}
    	
    	Optional<Client> optionalClient = clientsRepo.findById(Integer.parseInt(contractCreateRequest.getClientId()));
    	if (!optionalClient.isPresent()) {
			throw new NotFoundException();
    	}
    	
    	Optional<Place> optionalPlaceStart = placesRepo.findById(Integer.parseInt(contractCreateRequest.getPlaceStartId()));
    	if (!optionalPlaceStart.isPresent()) {
			throw new NotFoundException();
    	}
    	
    	Optional<Place> optionalPlaceEnd = placesRepo.findById(Integer.parseInt(contractCreateRequest.getPlaceEndId()));
    	if (!optionalPlaceEnd.isPresent()) {
			throw new NotFoundException();
    	}
    	
    	Car car = optionalCar.get();
    	Client client = optionalClient.get();
    	Place placeStart = optionalPlaceStart.get();  
    	Place placeEnd = optionalPlaceEnd.get();   	    	
    	
    	Timestamp createdTimestamp = Timestamp.valueOf(contractCreateRequest.getCreated());      	
    	Timestamp closedTimestamp = contractCreateRequest.getClosed().isEmpty() ? null : Timestamp.valueOf(contractCreateRequest.getClosed());
    	
    	if (closedTimestamp != null) {
        	if (!this.checkDate(createdTimestamp, closedTimestamp)) {
        		throw new ConflictException();
        	}
    	}

    	Contract contract = new Contract(createdTimestamp, closedTimestamp, car, client, placeStart, placeEnd);
    	contractsRepo.save(contract);
    	
    	return contract;
	}
	
	public Contract update(Integer id, ContractCreateRequest contractUpdateRequest) {
		Optional<Contract> optionalContract = contractsRepo.findById(id);
    	
    	if (!optionalContract.isPresent()) {
    		throw new NotFoundException();
    	}
    	
    	Contract contract = optionalContract.get();   
    	
    	Timestamp closedTimestamp = Timestamp.valueOf(contractUpdateRequest.getClosed());    
    	
    	if (!this.checkDate(contract.getCreated(), closedTimestamp)) {
    		throw new ConflictException();
    	}
    	
    	contract.setClosed(closedTimestamp);
    	
    	contractsRepo.save(contract);
    	
    	return contract;
	}
	
	public void delete(Integer id) {
		Optional<Contract> optionalContract = contractsRepo.findById(id);
    	
    	if (!optionalContract.isPresent()) {
    		throw new NotFoundException();
    	}

		Contract contract = optionalContract.get();
    	contractsRepo.delete(contract);
	}
	
	private Boolean checkDate(Timestamp createdTimestamp, Timestamp closedTimestamp) {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try {
			Date created = format.parse(format.format(createdTimestamp));
			Date closed = format.parse(format.format(closedTimestamp));
			
			Long diff =  closed.getTime() - created.getTime();
			
			if (diff > 0) {
				return true;
			}
			
			return false;
		} catch (Exception e) {
			return false;
		}
	}
}
