package carsharing.backend.models;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="contracts")
public class Contract {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Timestamp created;
	
	private Timestamp closed;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Car car;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Client client;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Place placeStart;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Place placeEnd;
	
	public Contract() {
	}
	
	public Contract(
			Timestamp created,
			Timestamp closed,
			Car car,
			Client client,
			Place placeStart,
			Place placeEnd
	) {
		this.created = created;
		this.closed = closed;
		this.car = car;
		this.client = client;
		this.placeStart = placeStart;
		this.placeEnd = placeEnd;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Timestamp getCreated() {
		return created;
	}
	
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	public Timestamp getClosed() {
		return closed;
	}
	
	public void setClosed(Timestamp closed) {
		this.closed = closed;
	}
	
	public Car getCar() {
		return car;
	}
	
	public void setCar(Car car) {
		this.car = car;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}
	
	public Place getPlaceStart() {
		return placeStart;
	}
	
	public void setPlaceStart(Place placeStart) {
		this.placeStart = placeStart;
	}
	
	public Place getPlaceEnd() {
		return placeEnd;
	}
	
	public void setPlaceEnd(Place placeEnd) {
		this.placeEnd = placeEnd;
	}
	
	public Boolean isClosed() {
		if (this.closed != null) {
			return true;
		}
		
		return false;
	}
}
