package carsharing.backend;

import carsharing.backend.models.Car;

public class CarDetails extends Car{
	private String rentalDuration;
	
	public CarDetails(Car car, String rentalDuration) {
		this.setId(car.getId());
		this.setCarModel(car.getCarModel());
		this.setCarNumber(car.getCarNumber());
		this.rentalDuration = rentalDuration;
	}
	
	public String getRentalDuration() {
		return this.rentalDuration;
	}
	
	public void setRentalDuration(String rentalDuration) {
		this.rentalDuration = rentalDuration;
	}
}
