package carsharing.backend;

public class ContractCreateRequest {
	private Integer id;
	
	private String created;
	
	private String closed;
	
	private String carId;
	
	private String clientId;
	
	private String placeStartId;
	
	private String placeEndId;
	
	public ContractCreateRequest() {
		
	}
			
	public ContractCreateRequest(
			String created,
			String closed,
			String carId,
			String clientId,
			String placeStartId,
			String placeEndId
	) {
		this.created = created;
		this.closed = closed;
		this.carId = carId;
		this.clientId = clientId;
		this.placeStartId = placeStartId;
		this.placeEndId = placeEndId;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCreated() {
		return created;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public String getClosed() {
		return closed;
	}
	
	public void setClosed(String closed) {
		this.closed = closed;
	}
	
	public String getCarId() {
		return carId;
	}
	
	public void setCarId(String carId) {
		this.carId = carId;
	}
	
	public String getClientId() {
		return clientId;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public String getPlaceStartId() {
		return placeStartId;
	}
	
	public void setPlaceStartId(String placeStartId) {
		this.placeStartId = placeStartId;
	}
	
	public String getPlaceEndId() {
		return placeEndId;
	}
	
	public void setPlaceEndId(String placeEndId) {
		this.placeEndId = placeEndId;
	}
}
