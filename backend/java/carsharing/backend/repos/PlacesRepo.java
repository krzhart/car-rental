package carsharing.backend.repos;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import carsharing.backend.models.Place;

public interface PlacesRepo extends CrudRepository<Place, Integer>{
	Optional<Place> findById(Integer id);
}
