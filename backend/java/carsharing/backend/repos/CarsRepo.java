package carsharing.backend.repos;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import carsharing.backend.Constants;
import carsharing.backend.models.Car;
import carsharing.backend.models.Client;

public interface CarsRepo extends CrudRepository<Car, Integer> {
	Optional<Car> findById(Integer id);
	
	@Query(value="SELECT * FROM " + Constants.SCHEMA + ".cars where id IN (SELECT car_id FROM " + Constants.SCHEMA + ".contracts c WHERE c.place_id = ?1)", nativeQuery = true)
	Set<Car> findByPlaceId(Integer placeId);
	
	@Query(value="SELECT * FROM " + Constants.SCHEMA + ".cars WHERE id NOT IN ( SELECT car_id FROM " + Constants.SCHEMA + ".contracts WHERE closed IS NULL)", nativeQuery = true)
	Set<Car> findFreeCars();
}
