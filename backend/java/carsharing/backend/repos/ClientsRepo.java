package carsharing.backend.repos;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import carsharing.backend.models.Client;

public interface ClientsRepo extends CrudRepository<Client, Integer> {
	Optional<Client> findById(Integer id);
}

