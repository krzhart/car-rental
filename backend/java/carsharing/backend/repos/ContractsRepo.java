package carsharing.backend.repos;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import carsharing.backend.models.Contract;

public interface ContractsRepo extends CrudRepository<Contract, Integer>{
	Optional<Contract> findById(Integer id);
	
	Set<Contract> findByCarId(Integer carId);
	Set<Contract> findByClientId(Integer clientId);
	Set<Contract> findByPlaceStartIdAndCarId(Integer placeStartId, Integer carId);
}
