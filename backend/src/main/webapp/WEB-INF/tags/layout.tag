<%@tag description="Layout for all pages"%>

<!-- Header -->
<%@attribute name="header" fragment="true" %>
<%@attribute name="subheader" fragment="true"%>

<!-- Footer -->
<%@attribute name="footer" fragment="true" %>

<!-- Title -->
<%@attribute name="title" required="true"%>

<!-- Body table -->
<%@attribute name="bodytable" fragment="true"%>

<html>
	<head>
		<meta charset="utf-8">
		
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">	
	    	
		<!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />	
 		<link rel="stylesheet" href="/webjars/font-awesome/4.7.0/css/font-awesome.min.css">

	    <!-- DataTables -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">		
		
		<!-- jQuery -->
	    <script src="https://code.jquery.com/jquery-3.4.1.js" ></script>
	    
		<!--  Bootstrap JS -->
	    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>	    
	    
	    <!-- DataTables -->
	    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	    
	    <!-- DateTimePicker -->
	    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
		
		
		<!-- Custom styles -->
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/sidebar.css">
		
		<!-- Custom js -->
		<script src="/js/main.js" charset="utf-8"></script>
		<script src="/js/sidebar.js" charset="utf-8"></script>
		
		<Title>${title}</Title>
	</head>
	<body>
		<div id="root">
			<jsp:include page="/WEB-INF/jsp/sidebar.jsp" />
			<div class="content">
				<header>
					<h2>
						<jsp:invoke fragment="header"/>
					</h2>				
					<div>
						<jsp:invoke fragment="subheader"/>
					</div>
				</header>
				<div class="wrapper my-5">
					<jsp:doBody/>
				</div>
				<jsp:invoke fragment="bodytable"/>
				<div id="pagefooter">
					<jsp:invoke fragment="footer"/>
				</div>
			</div>
		</div>	
	    
		
	</body>
</html>