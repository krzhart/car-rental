$(document).ready(function() {
	$('.date').datetimepicker({format: 'YYYY-MM-DD HH:mm:00'});	
	$('.table').DataTable({
		"bInfo" : false,
	    "bLengthChange": false,
	    "bFilter": true,
	    "bAutoWidth": false,
	    "language": {
		  "processing": "Подождите...",
		  "search": "Поиск:",
		  "lengthMenu": "Показать _MENU_ записей",
		  "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
		  "infoEmpty": "Записи с 0 до 0 из 0 записей",
		  "infoFiltered": "(отфильтровано из _MAX_ записей)",
		  "infoPostFix": "",
		  "loadingRecords": "Загрузка записей...",
		  "zeroRecords": "Записи отсутствуют.",
		  "emptyTable": "В таблице отсутствуют данные",
		  "paginate": {
		    "first": "Первая",
		    "previous": "Предыдущая",
		    "next": "Следующая",
		    "last": "Последняя"
		  },
		  "aria": {
		    "sortAscending": ": активировать для сортировки столбца по возрастанию",
		    "sortDescending": ": активировать для сортировки столбца по убыванию"
		  }
		}
	});
		
	//Set default values
	$('#clientId').val($('#clientSelect').val());
	$('#carId').val($('#carSelect').val());
	$('#placeStartId').val($('#placeStartSelect').val());
	$('#placeEndId').val($('#placeEndSelect').val());
	
	
	$('#clientSelect').change(() => {
		$('#clientId').val($('#clientSelect').val());
	});
	
	$('#carSelect').change(() => {
		$('#carId').val($('#carSelect').val());
	});
	
	$('#placeStartSelect').change(() => {
		$('#placeStartId').val($('#placeStartSelect').val());
	});

	$('#placeEndSelect').change(() => {
		$('#placeEndId').val($('#placeEndSelect').val());
	});
	
});