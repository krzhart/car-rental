<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout title="Клиенты">
    <jsp:attribute name="header">
      Клиенты
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		Клиенты
    </jsp:attribute>
    <jsp:body>		
		<div class="mb-5 content-buttons">
			<a class="btn btn-outline-primary" href="/clients/create">
				<span>Добавить</span>
			</a>
			<hr class="my-5">
		</div>		
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Фамилия</th>
					<th>Имя</th>
					<th>Отчество</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${clients}" var="client">
				     <tr>
				     	<td><a href="/clients/${client.id}"><c:out value="${client.id}" /></a></td>
				     	<td><c:out value="${client.lastName}" /></td>
				     	<td><c:out value="${client.firstName}" /></td>
				     	<td><c:out value="${client.middleName}" /></td>
				     	<td style="text-align: right;">
				     		<a class="text-warning" href="/clients/${client.id}/edit">Редактировать</a>
				     		<span class="ml-1 mr-1">|</span>
				     		<a class="text-danger" href="/clients/${client.id}/delete">Удалить</a>
			     		</td>
			        </tr>
			  	</c:forEach>
			</tbody>		  
		</table>
		 
    	<!-- menu item -->   	
		<script>
			setActive('clients');
		</script>
    </jsp:body>
</t:layout>
