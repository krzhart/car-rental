<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Клиент #${client.id}">
    <jsp:attribute name="header">
      Редактирование клиента
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/clients">Клиенты</a><span class="ml-1 mr-1">/</span>
   		Редактирование 
    </jsp:attribute>
    <jsp:body>    
    	<form class="container" method="post">
    		<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${client.id}</span>
				</div>
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="lastName" class="col-4 col-form-label">
					<span>Фамилия</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Фамилия" title="" value="${client.lastName}" id="lastName" name="lastName" required>
				</div>
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="firstName" class="col-4 col-form-label">
					<span>Имя</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Имя" title="" value="${client.firstName}" id="firstName" name="firstName" required>
				</div>
			</div>	
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="middleName" class="col-4 col-form-label">
					<span>Отчество</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Отчество" title="" value="${client.middleName}" id="middleName" name="middleName" required>
				</div>
			</div>		
			<hr class="my-5">
			<div class="col-auto offset-4">
				<button type="submit" class="btn btn-outline-success">Сохранить</button>
				<a class="btn btn-outline-danger" href="/clients"><span>Отмена</span></a>
			</div>
		</form>
		 
    	<!-- menu item -->   	
		<script>
			setActive('clients');
		</script>
    </jsp:body>
</t:layout>