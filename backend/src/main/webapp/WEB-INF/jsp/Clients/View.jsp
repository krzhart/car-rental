<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout title="Клиент #${client.id}">
    <jsp:attribute name="header">
    	Информация о клиенте
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/clients">Клиенты</a><span class="ml-1 mr-1">/</span>
   		${client.id}  
    </jsp:attribute>
    <jsp:attribute name="bodytable">   
    	<h3>История проката</h3>
		<div class="wrapper my-5">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Дата взятия</th>
						<th>Дата возврата</th>
						<th>Авто</th>
						<th>Точка взятия</th>
						<th>Точка возврата</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${contracts}" var="contract">
				     <tr>
				     	<td><a href="/contracts/${contract.id}"><c:out value="${contract.id}" /></a></td>
				     	<td>	
							<jsp:useBean id="createdDate" class="java.util.Date"/>
							<jsp:setProperty name="createdDate" property="time" value="${contract.created}"/>
							<fmt:formatDate value="${createdDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
						</td>
						<td>
							<c:if test="${contract.closed != null }">
								<jsp:useBean id="closedDate" class="java.util.Date"/>
								<jsp:setProperty name="closedDate" property="time" value="${contract.closed}"/>
								<fmt:formatDate value="${closedDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
							</c:if>		
						</td>
				     	<td>
				     		<a href="/cars/${contract.car.id}">
				     			<c:out value="${contract.car.carModel}" /> - <c:out value="${contract.car.carNumber}" /> 
				     		</a>
			     		</td>
			     		<td>
				     		<a href="/places/${contract.placeStart.id}">
				     			<c:out value="${contract.placeStart.address}" />
				     		</a>
			     		</td>
			     		<td>
				     		<a href="/places/${contract.placeEnd.id}">
				     			<c:out value="${contract.placeEnd.address}" />
				     		</a>
			     		</td>
			        </tr>
			  	</c:forEach>
				</tbody>		  
			</table>
		</div>
    </jsp:attribute>  
    <jsp:body>    		
    	<div class="mb-5 content-buttons">
			<a class="btn btn-outline-warning" href="/clients/${client.id}/edit">
				<span>Редактировать</span>
			</a>
			<a class="btn btn-outline-danger" href="/clients/${client.id}/delete">
				<span>Удалить</span>
			</a>
			<hr class="my-5">
		</div>	
		<div class="container">
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${client.id}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Фамилия</span>
				</div>
				<div class="col-8">
					<span>${client.lastName}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Имя</span>
				</div>
				<div class="col-8">
					<span>${client.firstName}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Отчество</span>
				</div>
				<div class="col-8">
					<span>${client.middleName}</span>
				</div>
			</div>
		</div>  
		 
    	<!-- menu item -->   	
		<script>
			setActive('clients');
		</script>
    </jsp:body>
</t:layout>