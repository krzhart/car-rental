<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:layout title="Точка проката #${place.id}">
    <jsp:attribute name="header">
    	Просмотр точки проката
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/places">Точки проката</a><span class="ml-1 mr-1">/</span>
   		${place.id}  
    </jsp:attribute>    
    <jsp:attribute name="bodytable">   
    	<h3>Автомобили</h3>
		<div class="wrapper my-5">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Модель</th>
						<th>Номер автомобиля</th>
						<th>Средняя продолжительность проката</th>
				</thead>
				<tbody>
					<c:forEach items="${cars}" var="car">
					     <tr>
					     	<td><a href="/cars/${car.id}"><c:out value="${car.id}" /></a></td>
					     	<td><c:out value="${car.carModel}" /></td>
					     	<td><c:out value="${car.carNumber}" /></td>
					     	<td><c:out value="${car.rentalDuration}" /></td>
				        </tr>
				  	</c:forEach>
				</tbody>		  
			</table>
		</div>
    </jsp:attribute>  
    <jsp:body>    		
    	<div class="mb-5 content-buttons">
			<a class="btn btn-outline-warning" href="/places/${place.id}/edit">
				<span>Редактировать</span>
			</a>
			<a class="btn btn-outline-danger" href="/places/${place.id}/delete">
				<span>Удалить</span>
			</a>
			<hr class="my-5">
		</div>	
		<div class="container">
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${place.id}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Адрес</span>
				</div>
				<div class="col-8">
					<span>${place.address}</span>
				</div>
			</div>
		</div>		 
    	<!-- menu item -->   	
		<script>
			setActive('places');
		</script>
    </jsp:body>  
</t:layout>