<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Точка проката #${place.id}">
    <jsp:attribute name="header">
      Редактирование точки проката
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/places">Точки проката</a><span class="ml-1 mr-1">/</span>
   		Редактирование 
    </jsp:attribute>
    <jsp:body>    
    	<form class="container" method="post">
    		<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${place.id}</span>
				</div>
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="address" class="col-4 col-form-label">
					<span>Адрес</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Адрес" title="" value="${place.address}" id="address" name="address" required>
				</div>
			</div>	
			<hr class="my-5">
			<div class="col-auto offset-4">
				<button type="submit" class="btn btn-outline-success">Сохранить</button>
				<a class="btn btn-outline-danger" href="/places"><span>Отмена</span></a>
			</div>
		</form>
		 
    	<!-- menu item -->   	
		<script>
			setActive('places');
		</script>
    </jsp:body>
</t:layout>