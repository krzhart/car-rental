<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="sidebar">
	<div class="sidebar-header">
		<a class="sidebar-logo" href="/contracts">Прокат автомобилей</span></a>
	</div>
	<div class="menu">
		<a class="menu-item" href="/contracts" id="contracts">
			<i class="icon fa fa-list-alt"></i>
			<span>История проката</span>
		</a>
		<a class="menu-item" href="/cars" id="cars">
			<i class="icon fa fa-car"></i>
			<span>Автомобили</span>
		</a>
		<a class="menu-item" href="/clients" id="clients">
			<i class="icon fa fa-male"></i>
			<span>Клиенты</span>
		</a>
		<a class="menu-item" href="/places" id="places">
			<i class="icon fa fa-building"></i>
			<span>Точки проката</span>
		</a>
	</div>
</div>