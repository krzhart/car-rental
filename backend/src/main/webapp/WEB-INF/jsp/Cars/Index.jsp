<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout title="Автомобили">
    <jsp:attribute name="header">
      Автомобили
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		Автомобили
    </jsp:attribute>
    <jsp:body>		
		<div class="mb-5 content-buttons">
			<a class="btn btn-outline-primary" href="/cars/create">
				<span>Добавить</span>
			</a>
			<hr class="my-5">
		</div>		
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Модель</th>
					<th>Номер автомобиля</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${cars}" var="car">
				     <tr>
				     	<td><a href="/cars/${car.id}"><c:out value="${car.id}" /></a></td>
				     	<td><c:out value="${car.carModel}" /></td>
				     	<td><c:out value="${car.carNumber}" /></td>
				     	<td style="text-align: right;">
				     		<a class="text-warning" href="/cars/${car.id}/edit">Редактировать</a>
				     		<span class="ml-1 mr-1">|</span>
				     		<a class="text-danger" href="/cars/${car.id}/delete">Удалить</a>
			     		</td>
			        </tr>
			  	</c:forEach>
			</tbody>		  
		</table>
		 
    	<!-- menu item -->   	
		<script>
			setActive('cars');
		</script>
    </jsp:body>
</t:layout>
