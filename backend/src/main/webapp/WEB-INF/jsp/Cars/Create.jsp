<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Новый автомобиль">
    <jsp:attribute name="header">
      Добавление автомобиля
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/cars">Автомобили</a><span class="ml-1 mr-1">/</span>
   		Новый 
    </jsp:attribute>
    <jsp:body>    
    	<form class="container" method="post">
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="carModel" class="col-4 col-form-label">
					<span>Модель</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Модель" title="" value="" id="carModel" name="carModel" required>
				</div>				
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="carNumber" class="col-4 col-form-label">
					<span>Номер</span>
				</label>
				<div class="col-8">
					<input class="form-control" type="text" placeholder="Номер" title="" value="" id="carNumber" name="carNumber" required>
				</div>
			</div>		
			<hr class="my-5">
			<div class="col-auto offset-4">
				<button type="submit" class="btn btn-outline-success">Добавить</button>
				<a class="btn btn-outline-danger" href="/cars"><span>Отмена</span></a>
			</div>
		</form>
		 
    	<!-- menu item -->   	
		<script>
			setActive('cars');
		</script>
    </jsp:body>
</t:layout>