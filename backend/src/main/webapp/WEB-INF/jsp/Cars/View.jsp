<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout title="Автомобиль #${car.id}">
    <jsp:attribute name="header">
    	Просмотр автомобиля
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
   		<a class="" href="/cars">Автомобили</a><span class="ml-1 mr-1">/</span>
   		${car.id}  
    </jsp:attribute>
     <jsp:attribute name="bodytable">   
    	<h3>История проката</h3>
		<div class="wrapper my-5">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Дата взятия</th>
						<th>Дата возврата</th>
						<th>Клиент</th>
						<th>Точка взятия</th>
						<th>Точка возврата</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${contracts}" var="contract">
				     <tr>
				     	<td><a href="/contracts/${contract.id}"><c:out value="${contract.id}" /></a></td>
				     	<td>	
							<jsp:useBean id="createdDate" class="java.util.Date"/>
							<jsp:setProperty name="createdDate" property="time" value="${contract.created}"/>
							<fmt:formatDate value="${createdDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
						</td>
						<td>
							<c:if test="${contract.closed != null }">
								<jsp:useBean id="closedDate" class="java.util.Date"/>
								<jsp:setProperty name="closedDate" property="time" value="${contract.closed}"/>
								<fmt:formatDate value="${closedDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
							</c:if>		
						</td>
				     	<td>
				     		<a href="/clients/${contract.client.id}">
					     		<c:out value="${contract.client.lastName}" />
					     		<c:out value="${contract.client.firstName}" /> 
					     		<c:out value="${contract.client.middleName}" />
				     		</a>
			     		</td>
			     		<td>
				     		<a href="/places/${contract.placeStart.id}">
				     			<c:out value="${contract.placeStart.address}" />
				     		</a>
			     		</td>
			     		<td>
				     		<a href="/places/${contract.placeEnd.id}">
				     			<c:out value="${contract.placeEnd.address}" />
				     		</a>
			     		</td>
			        </tr>
			  	</c:forEach>
				</tbody>		  
			</table>
		</div>
    </jsp:attribute>  
    <jsp:body>    		
    	<div class="mb-5 content-buttons">
			<a class="btn btn-outline-warning" href="/cars/${car.id}/edit">
				<span>Редактировать</span>
			</a>
			<a class="btn btn-outline-danger" href="/cars/${car.id}/delete">
				<span>Удалить</span>
			</a>
			<hr class="my-5">
		</div>	
		<div class="container">
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${car.id}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Модель</span>
				</div>
				<div class="col-8">
					<span>${car.carModel}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Номер</span>
				</div>
				<div class="col-8">
					<span>${car.carNumber}</span>
				</div>
			</div>
		</div>  
		 
    	<!-- menu item -->   	
		<script>
			setActive('cars');
		</script>
    </jsp:body>
</t:layout>