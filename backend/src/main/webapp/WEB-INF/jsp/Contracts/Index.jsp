<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="История проката">
    <jsp:attribute name="header">
      История проката
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
    	История проката
    </jsp:attribute>
    <jsp:body>		
		<div class="mb-5 content-buttons">
			<a class="btn btn-outline-primary" href="/contracts/create">
				<span>Добавить</span>
			</a>			
			<hr class="my-5">
		</div>	
		<div class="table-responsive">	
			<table class="ui celled table" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Дата взятия</th>
						<th>Дата возврата</th>
						<th>Авто</th>
						<th>Клиент</th>
						<th>Точка взятия</th>
						<th>Точка возврата</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${contracts}" var="contract">
						<tr>
							<td><a href="/contracts/${contract.id}"><c:out value="${contract.id}" /></a></td>
							<td>	
								<jsp:useBean id="createdDate" class="java.util.Date"/>
								<jsp:setProperty name="createdDate" property="time" value="${contract.created}"/>
								<fmt:formatDate value="${createdDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
							</td>
							<td>
								<c:if test="${contract.closed != null }">
									<jsp:useBean id="closedDate" class="java.util.Date"/>
									<jsp:setProperty name="closedDate" property="time" value="${contract.closed}"/>
									<fmt:formatDate value="${closedDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
								</c:if>		
							</td>
							<td>
								<a href="/cars/${contract.car.id}">
									<c:out value="${contract.car.carModel}" /> - <c:out value="${contract.car.carNumber}" /> 
								</a>
							</td>
							<td>
								<a href="/clients/${contract.client.id}">
									<c:out value="${contract.client.lastName}" />
									<c:out value="${contract.client.firstName}" /> 
									<c:out value="${contract.client.middleName}" />
								</a>
							</td>
							<td>
								<a href="/places/${contract.placeStart.id}">
									<c:out value="${contract.placeStart.address}" />
								</a>
							</td>
							<td>
								<a href="/places/${contract.placeEnd.id}">
									<c:out value="${contract.placeEnd.address}" />
								</a>
							</td>
							<td style="text-align: right;">
								<c:if test="${contract.closed == null }">
									<a class="text-warning" href="/contracts/${contract.id}/edit">Редактировать</a>
									<span class="ml-1 mr-1">|</span> 
								</c:if>				     		
								<a class="text-danger" href="/contracts/${contract.id}/delete">Удалить</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>		  
			</table>
		 </div>

    	<!-- menu item -->   	
		<script>
			setActive('contracts');
		</script>
    </jsp:body>
</t:layout>
