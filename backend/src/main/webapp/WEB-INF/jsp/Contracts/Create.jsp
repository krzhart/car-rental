<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:layout title="Новый договор">
    <jsp:attribute name="header">
      Новый договор
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
    	<a class="" href="/contracts">История проката</a><span class="ml-1 mr-1">/</span>
   		Новый договор 
    </jsp:attribute>
    <jsp:body>    
    	<form class="container" method="post">
   			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="created" class="col-4 col-form-label">
					<span>Дата взятия в прокат</span>
				</label>
				<div class="col-8">
	                <div class="input-group date" id="created" data-target-input="nearest">
	                    <input type="text" class="form-control datetimepicker-input" data-target="#created" name="created" required/>
	                    <div class="input-group-append" data-target="#created" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
	                </div>
		        </div>
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="closed" class="col-4 col-form-label">
					<span>Дата возврата из прокат</span>
				</label>
				<div class="col-8">
	                <div class="input-group date" id="closed" data-target-input="nearest">
	                    <input type="text" class="form-control datetimepicker-input" data-target="#closed" name="closed"/>
	                    <div class="input-group-append" data-target="#closed" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
	                </div>
		        </div>		
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="carId" class="col-4 col-form-label">
					<span>Автомобиль</span>
				</label>
				<div class="col-8">
					<select class="form-control" id="carSelect" required>
						<c:forEach items="${cars}" var="car">						
							<option value="${car.id}">
								<span>${car.carModel} - ${car.carNumber}</span>
							</option>
						</c:forEach>
   		 			</select>
					<input class="form-control" type="hidden" placeholder="Автомобиль" title="" value="" id="carId" name="carId" required>
				</div>				
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="clientId" class="col-4 col-form-label">
					<span>Клиент</span>
				</label>
				<div class="col-8">
					<select class="form-control" id="clientSelect" required>
						<c:forEach items="${clients}" var="client">
						
							<option value="${client.id}">
								<span>${client.lastName} ${client.firstName} ${client.middleName}</span>
							</option>
						</c:forEach>
   		 			</select>
					<input class="form-control" type="hidden" placeholder="Клиент" title="" value="" id="clientId" name="clientId" required>
				</div>				
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="placeStartId" class="col-4 col-form-label">
					<span>Точка взятия в прокат</span>
				</label>
				<div class="col-8">
					<select class="form-control" id="placeStartSelect" required>
						<c:forEach items="${places}" var="place">						
							<option value="${place.id}">
								<span>${place.address}</span>
							</option>
						</c:forEach>
   		 			</select>
					<input class="form-control" type="hidden" placeholder="Точка проката" title="" value="" id="placeStartId" name="placeStartId" required>
				</div>				
			</div>
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="placeEndId" class="col-4 col-form-label">
					<span>Точка возврата из проката</span>
				</label>
				<div class="col-8">
					<select class="form-control" id="placeEndSelect" required>
						<c:forEach items="${places}" var="place">						
							<option value="${place.id}">
								<span>${place.address}</span>
							</option>
						</c:forEach>
   		 			</select>
					<input class="form-control" type="hidden" placeholder="Точка проката" title="" value="" id="placeEndId" name="placeEndId" required>
				</div>				
			</div>

			<hr class="my-5">
			<div class="col-auto offset-4">
				<button type="submit" class="btn btn-outline-success">Добавить</button>
				<a class="btn btn-outline-danger" href="/contracts"><span>Отмена</span></a>
			</div>
		</form>
		 
    	<!-- menu item -->   	
		<script>
			setActive('contracts');
		</script>
    </jsp:body>
</t:layout>