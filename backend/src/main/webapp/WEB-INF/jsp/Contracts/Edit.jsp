<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout title="Договор #${contract.id}">
    <jsp:attribute name="header">
      Редактирование договора
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
    	<a class="" href="/contracts">История проката</a><span class="ml-1 mr-1">/</span>
   		Редактирование 
    </jsp:attribute>
    <jsp:body>    
    	<form class="container" method="post">	
    		<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID</span>
				</div>
				<div class="col-8">
					<span>${contract.id}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Клиент</span>
				</div>
				<div class="col-8">
					<a href="/clients/${contract.client.id}">
						<span>${contract.client.lastName}</span>
						<span>${contract.client.firstName}</span>
						<span>${contract.client.middleName}</span>
					</a>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Автомобиль</span>
				</div>
				<div class="col-8">
					<a href="/cars/${contract.car.id}">
						<span>${contract.car.carModel}</span> -
						<span>${contract.car.carNumber}</span>
					</a>
				</div>
			</div>
			<!-- Place -->
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Точка взятия в прокат</span>
				</div>
				<div class="col-8">
					<a href="/places/${contract.placeStart.id}"><span>${contract.placeStart.address}</span></a>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Точка возврата из проката</span>
				</div>
				<div class="col-8">
					<a href="/places/${contract.placeEnd.id}"><span>${contract.placeEnd.address}</span></a>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Дата взятия в прокат</span>
				</div>
				<div class="col-8">
					<span><fmt:formatDate value="${contract.created}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
				</div>
			</div>	
			<div class="form-group row mt-1 mb-1 pt-2 pb-2">
				<label for="closed" class="col-4 col-form-label">
					<span>Дата возврата в прокат</span>
				</label>
				<div class="col-8">
	                <div class="input-group date" id="closed" data-target-input="nearest">
	                    <input type="text" class="form-control datetimepicker-input" data-target="#closed" name="closed" required/>
	                    <div class="input-group-append" data-target="#closed" data-toggle="datetimepicker">
	                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                    </div>
	                </div>
		        </div>		
			</div>
			<hr class="my-5">
			<div class="col-auto offset-4">
				<button type="submit" class="btn btn-outline-success">Сохранить</button>
				<a class="btn btn-outline-danger" href="/contracts"><span>Отмена</span></a>
			</div>
		</form>
		 
    	<!-- menu item -->   	
		<script>
			setActive('contracts');
		</script>
    </jsp:body>
</t:layout>