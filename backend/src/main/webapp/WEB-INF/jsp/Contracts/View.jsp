<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout title="Договор #${contract.id}">
    <jsp:attribute name="header">
    	Детали договора
    </jsp:attribute>
    <jsp:attribute name="subheader">
    	<a class="" href="/contracts">Прокат</a><span class="ml-1 mr-1">/</span>
    	<a class="" href="/contracts">История проката</a><span class="ml-1 mr-1">/</span>
   		${contract.id}  
    </jsp:attribute>
    <jsp:body> 	
    	<div class="mb-5 content-buttons">
	    	<c:if test="${contract.closed == null }">   
		    	<a class="btn btn-outline-warning" href="/contracts/${contract.id}/edit">
					<span>Редактировать</span>
				</a>	
			</c:if>
			<a class="btn btn-outline-danger" href="/contracts/${contract.id}/delete">
				<span>Удалить</span>
			</a>
			<hr class="my-5">
		</div>	
		<div class="container">	
			<!-- Contract -->
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID договора</span>
				</div>
				<div class="col-8">
					<span>${contract.id}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Дата взятия в прокат</span>
				</div>
				<div class="col-8">
					<span><fmt:formatDate value="${contract.created}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Дата возврата в прокат</span>
				</div>
				<div class="col-8">
					<span><fmt:formatDate value="${contract.closed}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
				</div>
			</div>		
			<hr class="my-5">	
			
			<!-- Car -->
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>ID Автомобиля</span>
				</div>
				<div class="col-8">
					<a href="/cars/${contract.car.id}"><span>${contract.car.id}</span></a>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Модель</span>
				</div>
				<div class="col-8">
					<span>${contract.car.carModel}</span>
				</div>
			</div>
			<div class="row mt-1 mb-1 pt-2 pb-2">
				<div class="col-4">
					<span>Номер</span>
				</div>
				<div class="col-8">
					<span>${contract.car.carNumber}</span>
				</div>
				</div>
				<hr class="my-5">
				
				<!-- Client -->
				<div class="row mt-1 mb-1 pt-2 pb-2">
					<div class="col-4">
						<span>ID Клиента</span>
					</div>
					<div class="col-8">
						<a href="/clients/${contract.client.id}"><span>${contract.client.id}</span></a>
					</div>
				</div>			
				<div class="row mt-1 mb-1 pt-2 pb-2">
					<div class="col-4">
						<span>ФИО </span>
					</div>
					<div class="col-8">
						<span>${contract.client.lastName}</span>
						<span>${contract.client.firstName}</span>
						<span>${contract.client.middleName}</span>
					</div>
				</div>
				<hr class="my-5">	
				
				<!-- Place -->
				<div class="row mt-1 mb-1 pt-2 pb-2">
					<div class="col-4">
						<span>Точка взятия в прокат</span>
					</div>
					<div class="col-8">
						<a href="/places/${contract.placeStart.id}"><span>${contract.placeStart.address}</span></a>
					</div>
				</div>
				<div class="row mt-1 mb-1 pt-2 pb-2">
					<div class="col-4">
						<span>Точка возврата из проката</span>
					</div>
					<div class="col-8">
						<a href="/places/${contract.placeEnd.id}"><span>${contract.placeEnd.address}</span></a>
					</div>
				</div>
			</div>  
		 
    	<!-- menu item -->   	
		<script>
			setActive('contracts');
		</script>
    </jsp:body>
</t:layout>