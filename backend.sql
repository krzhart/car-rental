CREATE SCHEMA backend;

ALTER SCHEMA backend OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE SEQUENCE backend.cars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE	
    CACHE 1;
	
CREATE TABLE backend.cars (
    id integer DEFAULT nextval('backend.cars_id_seq'::regclass) NOT NULL,
    car_model character varying(255),
    car_number character varying(255)
);

ALTER TABLE backend.cars OWNER TO postgres;

ALTER TABLE ONLY backend.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);

CREATE SEQUENCE backend.clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE	
    CACHE 1;
	
CREATE TABLE backend.clients (
    id integer DEFAULT nextval('backend.clients_id_seq'::regclass) NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    middle_name character varying(255)
);

ALTER TABLE backend.clients OWNER TO postgres;

ALTER TABLE ONLY backend.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);
	
CREATE SEQUENCE backend.contracts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE	
    CACHE 1;
	
CREATE TABLE backend.contracts (
    id integer DEFAULT nextval('backend.contracts_id_seq'::regclass) NOT NULL,
    address character varying(255),
    car_id integer,
    client_id integer,
    closed timestamp without time zone,
    created timestamp without time zone,
    place_start_id integer,
    place_end_id integer
);

ALTER TABLE backend.contracts OWNER TO postgres;

ALTER TABLE ONLY backend.contracts
    ADD CONSTRAINT contracts_pkey PRIMARY KEY (id);
		
CREATE SEQUENCE backend.places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE	
    CACHE 1;

CREATE TABLE backend.places (
    id integer DEFAULT nextval('backend.places_id_seq'::regclass) NOT NULL,
    address character varying(255)
);

ALTER TABLE backend.places OWNER TO postgres;

ALTER TABLE ONLY backend.places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);
